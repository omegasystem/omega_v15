# -*- coding: utf-8 -*-
import geocoder
from itertools import chain
import werkzeug
from odoo import models, fields, api


USER_PRIVATE_FIELDS = ['password']
concat = chain.from_iterable


class LoginDetails(models.Model):
    _name = 'login.details'

    name = fields.Char(string="User")
    date_time = fields.Datetime(string="Login DateTime", default=lambda self: fields.datetime.now())
    ip_address = fields.Char(string="IP Address")
    city = fields.Char('City')
    state = fields.Char('State')
    country = fields.Char('Country')
    org = fields.Char('Org')
    zip = fields.Char('Zip')
    lat = fields.Char(compute='_get_user_location', string='Lat')
    long = fields.Char(compute='_get_user_location', string='Long')

    @api.depends('ip_address')
    def _get_user_location(self):
        for record in self:
            ip = geocoder.ip(record.ip_address)
            if ip:
                user_dict = ip.json
                record.lat = user_dict.get('lat')
                record.long = user_dict.get('lng')
                record.city = user_dict.get('city')
                record.state = user_dict.get('state')
                record.country = user_dict.get('country')
                record.org = user_dict.get('org')
                record.zip = user_dict.get('postal')
