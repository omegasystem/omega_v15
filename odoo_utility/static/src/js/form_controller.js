odoo.define('odoo_utility.form_controller', function(require) {
    "use strict";

    var core = require('web.core');
    var Widget= require('web.Widget');
    var widgetRegistry = require('web.widget_registry');
    var FieldManagerMixin = require('web.FieldManagerMixin');

    var OpenMap = Widget.extend({
        template: 'odoo_utility.open_map',
        xmlDependencies: ['/odoo_utility/static/src/xml/open_map.xml'],
        events: Object.assign({}, Widget.prototype.events, {
            'click .new_action_open_map': 'open_location',
        }),

        init: function (parent, model, state) {
            this._super.apply(this, arguments);
            this.modelName = model;
        },
        start: function () {
        },
        open_location: function () {
            var self = this;
            var latitude = this.modelName.data.lat;
            var longitude = this.modelName.data.long;
            var url = "https://maps.google.com/?q=" + latitude + "," + longitude;
            window.open(url);
        },
    });

    widgetRegistry.add(
        'open_map', OpenMap
);


});