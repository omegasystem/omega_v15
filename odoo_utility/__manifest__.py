{
    'name': 'Odoo Utility',
    'category': 'Extra Tools',
    'summary': '',
    'description': """
    """,
    'author': '',
    'website': '',
    'maintainer': '',

    'depends': [
        'web',
        'base',
        'base_geolocalize',
        'auth_signup',
        'website',
        'base_setup'
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/menu.xml',
        'views/ir_model.xml',
        'views/login_user_views.xml',
        'views/res_users.xml',
        'security/security.xml'
        # 'views/assets.xml',
        # 'views/res_users_view.xml',
        # 'views/res_partner_view.xml',
        # 'views/thankyou_signup.xml',
        # 'views/website.xml',
        # 'data/signup_template_data.xml',
        # 'views/auth_signup_view.xml',
        # 'views/res_config_setting_view.xml',
        # 'views/template.xml'
    ],
    'qweb': [
        "static/src/xml/*.xml"
    ],
    'assets': {
        'web.assets_backend': [
            '/odoo_utility/static/src/js/disable_quick_create.js',
            '/odoo_utility/static/src/js/form_controller.js',
        ],
        # 'web.assets_frontend': [
        #     '/odoo_utility/static/src/less/signup.less',
        # ],
        # 'web.assets_frontend': [
        #     '/odoo_utility/static/src/js/restrict_country.js',
        #     '/odoo_utility/static/src/js/maxmin.js'
        # ],
    },

    'price': 150.00,
    'currency': 'EUR',
    'installable': True,
    'application': True,
}
