# -*- coding: utf-8 -*-
from odoo import api, fields, models, _


class ExportStockInfoWiz(models.TransientModel):
    _name = 'stock.onhand.wiz'

    location_ids = fields.Many2many('stock.location', string="Location")
    category_ids = fields.Many2many('product.category', string="Category")

    def print_stock_onhand_report(self):
        return self.env.ref(
            'os_stock_onhand_report.action_stock_onhand_report').report_action(
            self)

    def get_onhand_products(self, loc):
        lst = []
        for rec in self:
            domain = [('location_id', 'child_of', [loc.id])]
            if rec.category_ids:
                domain.append(('product_id.categ_id', 'in',
                               [categ.id for categ in rec.category_ids]))
            locations = self.env['stock.quant'].search(domain)
            for prod in locations:
                lst.append({
                    'code': prod.product_id.default_code,
                    'product': prod.product_id.name,
                    'qty': prod.quantity,
                    'cost': prod.product_id.standard_price,
                    'total': round(
                        prod.quantity * prod.product_id.standard_price, 3),
                })
            return lst
