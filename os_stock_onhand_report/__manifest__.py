# -*- coding: utf-8 -*-
{
    'name': 'Inventory Onhand Report',
    'version': '15.0.1.0.0',
    'author': 'Omega System',
    'maintainer': 'Omega System',
    'website': 'https://omegasystem.in/',
    'license': 'AGPL-3',
    'category': 'stock',
    'summary': 'Inventory Onhand Report',
    'depends': [
        'sale',
        'stock'
    ],
    'data': [
        'security/ir.model.access.csv',
        'wizard/onhand_wizard_view.xml',
        'wizard/report.xml',
    ],
    'images': ['static/description/banner.gif'],
    'price': 12.00,
    'currency': 'EUR',
    'installable': True,
    'application': False,
}

