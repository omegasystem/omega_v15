# -*- coding: utf-8 -*-
{
    'name': 'Warehouse & Locations Restriction',
    'version': '15.0.1.0.0',
    'author': 'Omega System',
    'maintainer': 'Omega System',
    'website': 'https://omegasystem.in/',
    'license': 'AGPL-3',
    'category': 'Warehouse',
    'summary': 'Warehouse Restriction Users',
    'depends': [
        'stock'
    ],
    'data': [
        'security/group.xml',
        'views/stock_warehouse_views.xml',
        'views/stock_location_views.xml',
    ],
    'images': ['static/description/banner.gif'],
    'price': 12.00,
    'currency': 'EUR',
    'installable': True,
    'application': False,
}


