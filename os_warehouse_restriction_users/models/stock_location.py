# -*- coding: utf-8 -*-
from odoo import fields, models, api, SUPERUSER_ID, _
from odoo.exceptions import ValidationError


class StockLocation(models.Model):
    _inherit = 'stock.location'

    user_ids = fields.Many2many('res.users', string='Allowed Users')

    @api.constrains('user_ids')
    def check_restrict_users(self):
        self._check_location_users_restriction('user_ids')

    def _check_location_users_restriction(self, field):
        self.env['ir.rule'].clear_caches()
        env_user = self.env.user
        if env_user.id == SUPERUSER_ID:
            return True
        for rec in self.sudo():
            location_users = rec[field]
            if location_users and env_user not in location_users:
                raise ValidationError(_(
                    'You cannot restrict the "%s" '
                    'Location to users without including yourself as you'
                    'will no longer see this Location')
                                      % (rec.name))
        self.env.user.context_get.clear_cache(self)


class UsersLocation(models.Model):
    _inherit = 'res.users'

    location_id = fields.Many2many('stock.location', store=True)


class StockQuantity(models.Model):
    _inherit = 'stock.quant'

    user_ids = fields.Many2many('res.users', related='location_id.user_ids')
