# -*- coding: utf-8 -*-
from odoo import fields, models, api, SUPERUSER_ID, _
from odoo.exceptions import ValidationError


class StockWarehouse(models.Model):
    _inherit = 'stock.warehouse'

    user_ids = fields.Many2many('res.users', string='Allowed Users')

    @api.constrains('user_ids')
    def check_restrict_users(self):
        self._check_warehouse_users_restriction('user_ids')

    def _check_warehouse_users_restriction(self, field):
        self.env['ir.rule'].clear_caches()
        env_user = self.env.user
        if env_user.id == SUPERUSER_ID:
            return True
        for rec in self.sudo():
            warehouse_users = rec[field]
            if warehouse_users and env_user not in warehouse_users:
                raise ValidationError(_(
                    'You cannot restrict the "%s" '
                    'Warehouse to users without including yourself as '
                    'you will no longer see this Warehouse')
                                      % (rec.name))
        self.env.user.context_get.clear_cache(self)


class StockPickingType(models.Model):
    _inherit = 'stock.picking.type'

    warehouse_id = fields.Many2one(
        'stock.warehouse', 'Warehouse',
        ondelete='cascade',
        store=True,
        check_company=True
    )


class UsersWarehouse(models.Model):
    _inherit = 'res.users'

    warehouse_id = fields.Many2many('stock.warehouse', store=True)
