# -*- coding: utf-8 -*-
{
    'name': 'POS Receipt Invoice Report',
    'version': '15.0.1.0.0',
    'author': 'Omega System',
    'maintainer': 'Omega System',
    'website': 'https://omegasystem.in/',
    'license': 'AGPL-3',
    'category': 'account',
    'summary': 'Print POS Receipt in Invoice',
    'depends': [
        'account',
        'point_of_sale'
    ],
    'data': [
        'report/pos_template_invoice.xml',
        'report/report.xml',
    ],
    'images': ['static/description/banner.gif'],
    'price': 12.00,
    'currency': 'EUR',
    'installable': True,
    'application': False,
}

